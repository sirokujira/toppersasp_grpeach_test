/* kernel_cfg.h */
#ifndef TOPPERS_KERNEL_CFG_H
#define TOPPERS_KERNEL_CFG_H

#define TNUM_TSKID	7
#define TNUM_SEMID	8
#define TNUM_FLGID	0
#define TNUM_DTQID	0
#define TNUM_PDQID	0
#define TNUM_MBXID	0
#define TNUM_MTXID	0
#define TNUM_MPFID	0
#define TNUM_CYCID	1
#define TNUM_ALMID	1
#define TNUM_ISRID	0

#define LOGTASK	1
#define TASK1	2
#define TASK2	3
#define MAIN_TASK	4
#define SERIAL_RCV_SEM1	1
#define SERIAL_SND_SEM1	2
#define SERIAL_RCV_SEM2	3
#define SERIAL_SND_SEM2	4
#define SERIAL_RCV_SEM3	5
#define SERIAL_SND_SEM3	6
#define SERIAL_RCV_SEM4	7
#define SERIAL_SND_SEM4	8
#define CYCHDR1	1
#define ALMHDR1	1

#endif /* TOPPERS_KERNEL_CFG_H */

